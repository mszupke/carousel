package pl.com.adroit.soft.carousel.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Gallery;

@SuppressWarnings("deprecation")
public class ExtendedGallery extends Gallery {

	public ExtendedGallery(Context context, AttributeSet attrSet) {
		super(context, attrSet);
	}

	@Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
		return false;
	}
}
