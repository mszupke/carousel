package pl.com.adroit.soft.carousel.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CarouselAdapter extends BaseAdapter {
	
	private LayoutInflater inflater = null;
	private int pages;
	private Context context;
	
	public CarouselAdapter(Context c, int pages) {		
		this.context = c;
		this.pages = pages;
		
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return Integer.MAX_VALUE;
	}

	@Override
	public Object getItem(int position) {
		if(position >= pages) {
			position = position % pages;
		}
		return position;
	}

	@Override
	public long getItemId(int position) {
		if(position >= pages) {
			position = position % pages;
		}
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;

		if (view == null) {
			view = inflater.inflate(pl.com.adroit.soft.carousel.R.layout.page, parent, false);
		}

		if(position >= pages) {
			position = position % pages;
		}
		
		TextView tv = (TextView)view.findViewById(pl.com.adroit.soft.carousel.R.id.tv);
		tv.setText("" + position);
		
		return view;
	}

}
