package pl.com.adroit.soft.carousel;

import pl.com.adroit.soft.carousel.widget.CarouselAdapter;
import pl.com.adroit.soft.carousel.widget.ExtendedGallery;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;

public class MainActivity extends Activity {

	int pages = 22;
	private CarouselAdapter adapter;
	private final String TAG = "pl.com.adroit.soft.carousel.MainActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		ExtendedGallery gallery = (ExtendedGallery) findViewById(R.id.mygallery);
		adapter = new CarouselAdapter(this, pages);
		
		gallery.setAdapter(adapter);
		
		int position = (Integer.MAX_VALUE/2) - (Integer.MAX_VALUE/2) % pages;
		gallery.setSelection(position);
		Log.i(TAG, position + "");
	}
}
